# ApiJS

A JS package to make AJAX request.

## Features

* CommonJS, ES2015 modules and UMD builds
* Testing with [Jest](https://github.com/facebook/jest)
* Automatic code formatting with [Prettier](https://prettier.io)
