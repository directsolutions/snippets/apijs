import Headers from "./headers";

interface RequestOptions {
    url: string;
    domain?: string;
    credentials?: boolean;
    headers?: { [name: string]: string; };
    method?: string;
    timeout?: number;
    body?: any;
}

class Request {

    private readonly _options: RequestOptions;

    constructor(options: string | RequestOptions) {
        // If string given, assume it is the url
        if (typeof options === "string") {
            options = {
                url: options
            };
        }

        // Check required options properties
        if (typeof options !== "object") {
            throw new TypeError(
                "Expected options to be string, got: " + typeof options
            );
        }
        if (typeof options.url !== "string") {
            throw new TypeError(
                "Expected url to be string, got: " + typeof options.url
            );
        }

        const url = typeof options.domain === "string" ? options.domain + options.url : options.url;
        const credentials = options.credentials;
        const headers = options.headers;
        const method = options.method;
        const timeout = options.timeout;
        const body = options.body ? JSON.parse(JSON.stringify(options.body)) : undefined;

        this._options = { url, credentials, headers, method, timeout, body };
    }

    get credentials(): boolean {
        return this._options.credentials || true;
    }

    get url(): string {
        return this._options.url;
    }

    get headers(): Headers {
        return new Headers(this._options.headers, true);
    }

    get method(): string {
        return typeof this._options.method === 'string' ? this._options.method.toUpperCase() : 'GET';
    }

    get body(): any {
        return this._options.body ? JSON.parse(JSON.stringify(this._options.body)) : undefined;
    }

    get hasBody(): boolean {
        return this._options.body ? true : false;
    }

    get timeout(): number {
        return this._options.timeout || 30;
    }
}

export default Request;
