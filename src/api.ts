import Headers from './headers';
import Request from './request';

interface HeadersObject {
    [name: string]: string;
}

interface APISettings {
    /** The base domain of every ajax request, except if overriden */
    domain?: string;
    /** The handler function for every ajax **response** */
    handler ?: 'default' | APIHandler;
    /** The headers to send for every ajax request */
    headers ?: HeadersObject;
}

interface ParsedResponse {
    status: number;
    statusText: string;
    data: any;
    raw: string;
    type: XMLHttpRequestResponseType;
    url: string;
    credentials: boolean;
    responseHeaders: HeadersObject;
    request: Request;
    xhr: XMLHttpRequest;
}

interface AJAXParams {
    /** The url of the request. Appended after the domain */
    url: string;
    /** Method to use for the request */
    method?: string;
    /** The body to send if POST etc, otherwise if GET request, then serialized and appended as query parameters */
    body?: any;
    /** The headers to send along with the api headers */
    headers?: HeadersObject;
    /** The timeout of the request in **seconds** */
    timeout?: number;
    /** Wether to enable credentials (cookies and such) */
    credentials?: boolean;
}

interface RegisterAJAXParams extends AJAXParams {
    hasBody: boolean;
}

type APIHandler = (res: ParsedResponse) => any;

/**
 * @class API
 * @classdesc An XMLHttpRequest wrapper to easily make AJAX requests
 */
class API {

    static dirname: string;
    static Headers: typeof Headers = Headers;
    static Request: typeof Request = Request;

    private _domain: string;
    public routes: {
        [name: string]: Function;
    }
    private _handler: Function | null;
    private headers: Headers;

    [route: string]: any;

    constructor(settings?: string | APISettings) {
        // If settings is string, then it is the domain
        if (typeof settings === 'string') {
            settings = {
                domain: settings
            };
        }

        // Set settings to object if not already to avoid unnecessary errors
        if (typeof settings !== 'object') {
            settings = {};
        }

        // The baseurl-domain for the API requests
        this._domain = settings.domain || '';

        // Object to save the routes
        this.routes = {};

        // Handler for response data
        this._handler = null;

        // Pass the headers parameter to the Headers class
        this.headers = new Headers(settings.headers);

        // Use setHandler, all logic is done in setHandler function
        this.setHandler(settings.handler || 'default');
    }

    /**
     * Sets handler to parse the response for every API request
     */
    setHandler(handler: APIHandler | 'default'): this {
        if (handler === 'default' || typeof handler === 'undefined') {
            this._handler = (res: ParsedResponse) => {
                if (res.data instanceof Object) {
                    if (res.data.statusCode == undefined) {
                        res.data.statusCode = res.status;
                    }
                    return res.data;
                }
                return {
                    success: false,
                    description: res.statusText,
                    statusCode: res.status,
                    dev: res
                };
            };
        } else if (typeof handler === 'function') {
            this._handler = handler;
        } else {
            this._handler = null;
        }
        return this;
    }

    /**
     * Registers a new route with the name given
     */
    registerRoute(name: string, args: Function | RegisterAJAXParams | string, binder: any = this): this {
        const self = this;
        if (
            typeof this[name] !== 'undefined' &&
            typeof this.route[name] === 'undefined'
        ) {
            throw new Error("Route name conflicts with this object's property");
        }
        if (typeof args === 'function') {
            this.routes[name] = args.bind(binder);
        } else if (typeof args === 'object' && args.hasBody) {
            this.routes[name] = function(params: any, callback: PromiseLike<any>) {
                return self.ajax({
                    url: args.url,
                    body: params,
                    credentials: args.credentials,
                    headers: args.headers,
                    method: args.method,
                    timeout: args.timeout
                }, callback);
            };
        } else {
            this.routes[name] = function(callback: PromiseLike<any>) {
                return self.ajax(args, callback);
            };
        }
        this[name] = this.routes[name];
        return this;
    }

    /**
     * Takes in a XMLHttpRequest and parse it to a standard format.
     */
    parseResponse(xhr: XMLHttpRequest, request: Request): ParsedResponse {
        const headers = new Headers(xhr.getAllResponseHeaders());
        let data = xhr.response,
            type = xhr.responseType;
        if (headers.has('content-type')) {
            const value = headers.get('content-type');
            if (value && value.indexOf('application/json') > -1) {
                type = 'json';
                try {
                    data = JSON.parse(data);
                } catch (e) {
                    data = null;
                }
            } else if (value && value.indexOf('text/html') > -1) {
                type = 'text';
            }
        }

        return {
            status: xhr.status,
            statusText: xhr.statusText,
            data: data,
            raw: xhr.responseText,
            type: type,
            url: xhr.responseURL,
            credentials: xhr.withCredentials,
            responseHeaders: headers.getAll(),
            request: request,
            xhr: xhr
        };
    }

    /**
     * Function which take in an object and transforms it to a single string,
     * so it can be sent using POST
     *
     * Taken from: https://stackoverflow.com/a/1714899
     *
     * @example serialize({name: 'alex'}) === 'name=alex'
     *
     * @example serialize({names: ['alex', 'leo']}) === 'names%5B0%5D=alex&names%5B1%5D=leo'
     *          Decoded: 'names[0]=alex&names[1]=leo'
     *
     * @example serialize({ name: 'alex' }, 'customer') === 'customer%5Bname%5D=alex',
     *          Decoded: 'customer[name]=alex'
     *
     * @returns {string} The input obj serialized
     */
    serialize(obj: any, prefix?: string): string {
        let str = [],
            p;
        for (p in obj) {
            if (obj.hasOwnProperty(p)) {
                let k = prefix ? prefix + '[' + p + ']' : p,
                    v = obj[p];
                str.push(
                    v !== null && typeof v === 'object'
                        ? this.serialize(v, k)
                        : encodeURIComponent(k) + '=' + encodeURIComponent(v)
                );
            }
        }
        return str.join('&');
    }

    /**
     * Basic async function which sends the http request and returns
     * the result or the error status
     */
    ajax(params: string | AJAXParams, callback?: PromiseLike<any>) {
        const self = this;
        let newParams: AJAXParams;

        if (typeof params === 'string') {
            newParams = {
                url: params
            };
        } else {
            newParams = params;
        }

        const request = new Request(
            Object.assign(
                {
                    domain: self._domain
                },
                newParams,
                {
                    headers: <HeadersObject>Object.assign(
                        this.headers.getAll(),
                        newParams.headers
                    )
                }
            )
        );

        // Create xhr object
        const xhr = new XMLHttpRequest();
        const p = new Promise(resolve => {
            // Open connection with request method and url
            if (request.method === 'get' && request.hasBody) {
                xhr.open(
                    request.method,
                    request.url + '?' + self.serialize(request.body)
                );
            } else {
                xhr.open(request.method, request.url);
            }

            // Set the AJAX timeout
            xhr.timeout = (request.timeout || 30) * 1000;

            // Let the request send credentials (CORS)
            xhr.withCredentials = request.credentials;

            // If method 'GET' use header to denote that the data to send will be url-encoded
            if (request.method.toLowerCase() !== 'get') {
                xhr.setRequestHeader(
                    'Content-Type',
                    'application/x-www-form-urlencoded; charset=UTF-8'
                );
            }

            // Set header for every headers key, value where the value is 'truthy'
            request.headers.getKeys().forEach(key => {
                const value = request.headers.get(key);
                if (value)
                    xhr.setRequestHeader(key, value);
            });

            const finishedFn = function() {
                let response = self.parseResponse(xhr, request);

                // If handler is a function, then pass the response through the handler first
                if (typeof self._handler === 'function') {
                    response = self._handler(response);
                }

                resolve(response);
            };

            // onload denotes success, so use resolve
            xhr.onload = finishedFn;

            // onerror denotes unsuccessful, so use reject
            xhr.onerror = finishedFn;

            // Send the request and serialize the data if any
            if (request.hasBody) {
                xhr.send(this.serialize(request.body));
            } else {
                xhr.send();
            }
        });

        // If a callback is defined then call it
        if (typeof callback === 'function') {
            p.then(callback);
        }

        // Returns the promise
        return p;
    }

    test() {
        return true;
    }
}

// Find script location if loaded with browser
(function() {
    if (typeof document === 'undefined') {
        return;
    }

    const scripts = document.getElementsByTagName('script');
    if (scripts.length > 0) {
        const src = scripts[scripts.length - 1].src;
        if (src && (src.includes('api.js') || src.includes('api.min.js'))) {
            API.dirname = src;
        }
    }

    // @ts-ignore
    if (typeof __filename === 'string') {
        // @ts-ignore
        API.dirname = __filename;
    }
})();

export default API;
