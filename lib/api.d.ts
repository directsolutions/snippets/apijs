import Headers from './headers';
import Request from './request';
interface HeadersObject {
    [name: string]: string;
}
interface APISettings {
    /** The base domain of every ajax request, except if overriden */
    domain?: string;
    /** The handler function for every ajax **response** */
    handler?: 'default' | APIHandler;
    /** The headers to send for every ajax request */
    headers?: HeadersObject;
}
interface ParsedResponse {
    status: number;
    statusText: string;
    data: any;
    raw: string;
    type: XMLHttpRequestResponseType;
    url: string;
    credentials: boolean;
    responseHeaders: HeadersObject;
    request: Request;
    xhr: XMLHttpRequest;
}
interface AJAXParams {
    /** The url of the request. Appended after the domain */
    url: string;
    /** Method to use for the request */
    method?: string;
    /** The body to send if POST etc, otherwise if GET request, then serialized and appended as query parameters */
    body?: any;
    /** The headers to send along with the api headers */
    headers?: HeadersObject;
    /** The timeout of the request in **seconds** */
    timeout?: number;
    /** Wether to enable credentials (cookies and such) */
    credentials?: boolean;
}
interface RegisterAJAXParams extends AJAXParams {
    hasBody: boolean;
}
declare type APIHandler = (res: ParsedResponse) => any;
/**
 * @class API
 * @classdesc An XMLHttpRequest wrapper to easily make AJAX requests
 */
declare class API {
    static dirname: string;
    static Headers: typeof Headers;
    static Request: typeof Request;
    private _domain;
    routes: {
        [name: string]: Function;
    };
    private _handler;
    private headers;
    [route: string]: any;
    constructor(settings?: string | APISettings);
    /**
     * Sets handler to parse the response for every API request
     */
    setHandler(handler: APIHandler | 'default'): this;
    /**
     * Registers a new route with the name given
     */
    registerRoute(name: string, args: Function | RegisterAJAXParams | string, binder?: any): this;
    /**
     * Takes in a XMLHttpRequest and parse it to a standard format.
     */
    parseResponse(xhr: XMLHttpRequest, request: Request): ParsedResponse;
    /**
     * Function which take in an object and transforms it to a single string,
     * so it can be sent using POST
     *
     * Taken from: https://stackoverflow.com/a/1714899
     *
     * @example serialize({name: 'alex'}) === 'name=alex'
     *
     * @example serialize({names: ['alex', 'leo']}) === 'names%5B0%5D=alex&names%5B1%5D=leo'
     *          Decoded: 'names[0]=alex&names[1]=leo'
     *
     * @example serialize({ name: 'alex' }, 'customer') === 'customer%5Bname%5D=alex',
     *          Decoded: 'customer[name]=alex'
     *
     * @returns {string} The input obj serialized
     */
    serialize(obj: any, prefix?: string): string;
    /**
     * Basic async function which sends the http request and returns
     * the result or the error status
     */
    ajax(params: string | AJAXParams, callback?: PromiseLike<any>): Promise<{}>;
    test(): boolean;
}
export default API;
