interface HeadersObject {
    [name: string]: string;
}
declare class Headers {
    private readonly headers;
    constructor(headers?: string | HeadersObject, lock?: boolean);
    /**
     * Returns the headers
     *
     * @returns {Object} headers
     */
    getAll(): HeadersObject;
    /**
     * Returns all of the header keys
     */
    getKeys(): string[];
    /**
     * Returns the value of a header from a given header key/name
     *
     * @param {string} key The header key/name
     *
     * @returns {string} The header value
     */
    get(key: string): string | undefined;
    /**
     * Sets or overrides a header key/name with the value given
     * and returns the object for easy chaining
     *
     * @example headers
     *          .setHeader('My-first-header', 'first-value')
     *          .setHeader('My-second-header', 'second-value')
     *          .setHeader(...
     */
    set(key: string, val: string): this;
    /**
     * Removes the header with the given key/name
     */
    remove(key: string): this;
    /**
     * Returns whether the header with the given key/name exists
     */
    has(key: string): boolean;
}
export default Headers;
