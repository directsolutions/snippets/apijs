class Headers {
    constructor(headers, lock = false) {
        if (headers instanceof Headers) {
            this.headers = headers.getAll();
        }
        else {
            this.headers = {};
            if (typeof headers === "object") {
                if (headers instanceof Array) {
                    headers.forEach(header => {
                        if (typeof header !== "string" ||
                            header.lastIndexOf(":") < 0) {
                            return;
                        }
                        let i = header.lastIndexOf(":");
                        this.headers[header.substring(0, i).trim()] = header.substring(i + 1).trim();
                    });
                }
                else if (headers && headers instanceof Object) {
                    Object.keys(headers).forEach(key => {
                        if (typeof key === "string" &&
                            typeof headers[key] === "string") {
                            this.headers[key.trim().toLowerCase()] = headers[key].trim();
                        }
                    });
                }
            }
            else if (typeof headers === "string") {
                headers.split("\r\n").forEach(header => {
                    let i = header.indexOf(":");
                    if (i < 0) {
                        return;
                    }
                    this.headers[header
                        .substring(0, i)
                        .trim()
                        .toLowerCase()] = header.substring(i + 1).trim();
                });
            }
        }
        if (lock) {
            Object.freeze(this.headers);
        }
    }
    /**
     * Returns the headers
     *
     * @returns {Object} headers
     */
    getAll() {
        const headers = {};
        Object.keys(this.headers).forEach(key => {
            if (typeof key !== 'string') {
                return;
            }
            const value = this.headers[key];
            if (typeof value !== 'string') {
                return;
            }
            headers[key] = value;
        });
        return headers;
    }
    /**
     * Returns all of the header keys
     */
    getKeys() {
        return Object.keys(this.headers);
    }
    /**
     * Returns the value of a header from a given header key/name
     *
     * @param {string} key The header key/name
     *
     * @returns {string} The header value
     */
    get(key) {
        return this.headers[key.toLowerCase()];
    }
    /**
     * Sets or overrides a header key/name with the value given
     * and returns the object for easy chaining
     *
     * @example headers
     *          .setHeader('My-first-header', 'first-value')
     *          .setHeader('My-second-header', 'second-value')
     *          .setHeader(...
     */
    set(key, val) {
        if (typeof key !== "string" || typeof val !== "string") {
            return this;
        }
        this.headers[key.toLowerCase()] = val;
        return this;
    }
    /**
     * Removes the header with the given key/name
     */
    remove(key) {
        if (typeof key !== "string" ||
            typeof this.headers[key.toLowerCase()] !== "string") {
            return this;
        }
        if (delete this.headers[key.toLowerCase()]) {
            return this;
        }
        this.headers[key.toLowerCase()] = undefined;
        return this;
    }
    /**
     * Returns whether the header with the given key/name exists
     */
    has(key) {
        if (typeof key !== "string") {
            return false;
        }
        return this.headers[key.toLowerCase()] ? true : false;
    }
}
export default Headers;
