import Headers from "./headers";
interface RequestOptions {
    url: string;
    domain?: string;
    credentials?: boolean;
    headers?: {
        [name: string]: string;
    };
    method?: string;
    timeout?: number;
    body?: any;
}
declare class Request {
    private readonly _options;
    constructor(options: string | RequestOptions);
    readonly credentials: boolean;
    readonly url: string;
    readonly headers: Headers;
    readonly method: string;
    readonly body: any;
    readonly hasBody: boolean;
    readonly timeout: number;
}
export default Request;
