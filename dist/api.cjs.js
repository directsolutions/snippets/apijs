'use strict';

class Headers {
    constructor(headers, lock = false) {
        if (headers instanceof Headers) {
            this.headers = headers.getAll();
        }
        else {
            this.headers = {};
            if (typeof headers === "object") {
                if (headers instanceof Array) {
                    headers.forEach(header => {
                        if (typeof header !== "string" ||
                            header.lastIndexOf(":") < 0) {
                            return;
                        }
                        let i = header.lastIndexOf(":");
                        this.headers[header.substring(0, i).trim()] = header.substring(i + 1).trim();
                    });
                }
                else if (headers && headers instanceof Object) {
                    Object.keys(headers).forEach(key => {
                        if (typeof key === "string" &&
                            typeof headers[key] === "string") {
                            this.headers[key.trim().toLowerCase()] = headers[key].trim();
                        }
                    });
                }
            }
            else if (typeof headers === "string") {
                headers.split("\r\n").forEach(header => {
                    let i = header.indexOf(":");
                    if (i < 0) {
                        return;
                    }
                    this.headers[header
                        .substring(0, i)
                        .trim()
                        .toLowerCase()] = header.substring(i + 1).trim();
                });
            }
        }
        if (lock) {
            Object.freeze(this.headers);
        }
    }
    /**
     * Returns the headers
     *
     * @returns {Object} headers
     */
    getAll() {
        const headers = {};
        Object.keys(this.headers).forEach(key => {
            if (typeof key !== 'string') {
                return;
            }
            const value = this.headers[key];
            if (typeof value !== 'string') {
                return;
            }
            headers[key] = value;
        });
        return headers;
    }
    /**
     * Returns all of the header keys
     */
    getKeys() {
        return Object.keys(this.headers);
    }
    /**
     * Returns the value of a header from a given header key/name
     *
     * @param {string} key The header key/name
     *
     * @returns {string} The header value
     */
    get(key) {
        return this.headers[key.toLowerCase()];
    }
    /**
     * Sets or overrides a header key/name with the value given
     * and returns the object for easy chaining
     *
     * @example headers
     *          .setHeader('My-first-header', 'first-value')
     *          .setHeader('My-second-header', 'second-value')
     *          .setHeader(...
     */
    set(key, val) {
        if (typeof key !== "string" || typeof val !== "string") {
            return this;
        }
        this.headers[key.toLowerCase()] = val;
        return this;
    }
    /**
     * Removes the header with the given key/name
     */
    remove(key) {
        if (typeof key !== "string" ||
            typeof this.headers[key.toLowerCase()] !== "string") {
            return this;
        }
        if (delete this.headers[key.toLowerCase()]) {
            return this;
        }
        this.headers[key.toLowerCase()] = undefined;
        return this;
    }
    /**
     * Returns whether the header with the given key/name exists
     */
    has(key) {
        if (typeof key !== "string") {
            return false;
        }
        return this.headers[key.toLowerCase()] ? true : false;
    }
}

class Request {
    constructor(options) {
        // If string given, assume it is the url
        if (typeof options === "string") {
            options = {
                url: options
            };
        }
        // Check required options properties
        if (typeof options !== "object") {
            throw new TypeError("Expected options to be string, got: " + typeof options);
        }
        if (typeof options.url !== "string") {
            throw new TypeError("Expected url to be string, got: " + typeof options.url);
        }
        const url = typeof options.domain === "string" ? options.domain + options.url : options.url;
        const credentials = options.credentials;
        const headers = options.headers;
        const method = options.method;
        const timeout = options.timeout;
        const body = options.body ? JSON.parse(JSON.stringify(options.body)) : undefined;
        this._options = { url, credentials, headers, method, timeout, body };
    }
    get credentials() {
        return this._options.credentials || true;
    }
    get url() {
        return this._options.url;
    }
    get headers() {
        return new Headers(this._options.headers, true);
    }
    get method() {
        return typeof this._options.method === 'string' ? this._options.method.toUpperCase() : 'GET';
    }
    get body() {
        return this._options.body ? JSON.parse(JSON.stringify(this._options.body)) : undefined;
    }
    get hasBody() {
        return this._options.body ? true : false;
    }
    get timeout() {
        return this._options.timeout || 30;
    }
}

/**
 * @class API
 * @classdesc An XMLHttpRequest wrapper to easily make AJAX requests
 */
class API {
    constructor(settings) {
        // If settings is string, then it is the domain
        if (typeof settings === 'string') {
            settings = {
                domain: settings
            };
        }
        // Set settings to object if not already to avoid unnecessary errors
        if (typeof settings !== 'object') {
            settings = {};
        }
        // The baseurl-domain for the API requests
        this._domain = settings.domain || '';
        // Object to save the routes
        this.routes = {};
        // Handler for response data
        this._handler = null;
        // Pass the headers parameter to the Headers class
        this.headers = new Headers(settings.headers);
        // Use setHandler, all logic is done in setHandler function
        this.setHandler(settings.handler || 'default');
    }
    /**
     * Sets handler to parse the response for every API request
     */
    setHandler(handler) {
        if (handler === 'default' || typeof handler === 'undefined') {
            this._handler = (res) => {
                if (res.data instanceof Object) {
                    if (res.data.statusCode == undefined) {
                        res.data.statusCode = res.status;
                    }
                    return res.data;
                }
                return {
                    success: false,
                    description: res.statusText,
                    statusCode: res.status,
                    dev: res
                };
            };
        }
        else if (typeof handler === 'function') {
            this._handler = handler;
        }
        else {
            this._handler = null;
        }
        return this;
    }
    /**
     * Registers a new route with the name given
     */
    registerRoute(name, args, binder = this) {
        const self = this;
        if (typeof this[name] !== 'undefined' &&
            typeof this.route[name] === 'undefined') {
            throw new Error("Route name conflicts with this object's property");
        }
        if (typeof args === 'function') {
            this.routes[name] = args.bind(binder);
        }
        else if (typeof args === 'object' && args.hasBody) {
            this.routes[name] = function (params, callback) {
                return self.ajax({
                    url: args.url,
                    body: params,
                    credentials: args.credentials,
                    headers: args.headers,
                    method: args.method,
                    timeout: args.timeout
                }, callback);
            };
        }
        else {
            this.routes[name] = function (callback) {
                return self.ajax(args, callback);
            };
        }
        this[name] = this.routes[name];
        return this;
    }
    /**
     * Takes in a XMLHttpRequest and parse it to a standard format.
     */
    parseResponse(xhr, request) {
        const headers = new Headers(xhr.getAllResponseHeaders());
        let data = xhr.response, type = xhr.responseType;
        if (headers.has('content-type')) {
            const value = headers.get('content-type');
            if (value && value.indexOf('application/json') > -1) {
                type = 'json';
                try {
                    data = JSON.parse(data);
                }
                catch (e) {
                    data = null;
                }
            }
            else if (value && value.indexOf('text/html') > -1) {
                type = 'text';
            }
        }
        return {
            status: xhr.status,
            statusText: xhr.statusText,
            data: data,
            raw: xhr.responseText,
            type: type,
            url: xhr.responseURL,
            credentials: xhr.withCredentials,
            responseHeaders: headers.getAll(),
            request: request,
            xhr: xhr
        };
    }
    /**
     * Function which take in an object and transforms it to a single string,
     * so it can be sent using POST
     *
     * Taken from: https://stackoverflow.com/a/1714899
     *
     * @example serialize({name: 'alex'}) === 'name=alex'
     *
     * @example serialize({names: ['alex', 'leo']}) === 'names%5B0%5D=alex&names%5B1%5D=leo'
     *          Decoded: 'names[0]=alex&names[1]=leo'
     *
     * @example serialize({ name: 'alex' }, 'customer') === 'customer%5Bname%5D=alex',
     *          Decoded: 'customer[name]=alex'
     *
     * @returns {string} The input obj serialized
     */
    serialize(obj, prefix) {
        let str = [], p;
        for (p in obj) {
            if (obj.hasOwnProperty(p)) {
                let k = prefix ? prefix + '[' + p + ']' : p, v = obj[p];
                str.push(v !== null && typeof v === 'object'
                    ? this.serialize(v, k)
                    : encodeURIComponent(k) + '=' + encodeURIComponent(v));
            }
        }
        return str.join('&');
    }
    /**
     * Basic async function which sends the http request and returns
     * the result or the error status
     */
    ajax(params, callback) {
        const self = this;
        let newParams;
        if (typeof params === 'string') {
            newParams = {
                url: params
            };
        }
        else {
            newParams = params;
        }
        const request = new Request(Object.assign({
            domain: self._domain
        }, newParams, {
            headers: Object.assign(this.headers.getAll(), newParams.headers)
        }));
        // Create xhr object
        const xhr = new XMLHttpRequest();
        const p = new Promise(resolve => {
            // Open connection with request method and url
            if (request.method === 'get' && request.hasBody) {
                xhr.open(request.method, request.url + '?' + self.serialize(request.body));
            }
            else {
                xhr.open(request.method, request.url);
            }
            // Set the AJAX timeout
            xhr.timeout = (request.timeout || 30) * 1000;
            // Let the request send credentials (CORS)
            xhr.withCredentials = request.credentials;
            // If method 'GET' use header to denote that the data to send will be url-encoded
            if (request.method.toLowerCase() !== 'get') {
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            }
            // Set header for every headers key, value where the value is 'truthy'
            request.headers.getKeys().forEach(key => {
                const value = request.headers.get(key);
                if (value)
                    xhr.setRequestHeader(key, value);
            });
            const finishedFn = function () {
                let response = self.parseResponse(xhr, request);
                // If handler is a function, then pass the response through the handler first
                if (typeof self._handler === 'function') {
                    response = self._handler(response);
                }
                resolve(response);
            };
            // onload denotes success, so use resolve
            xhr.onload = finishedFn;
            // onerror denotes unsuccessful, so use reject
            xhr.onerror = finishedFn;
            // Send the request and serialize the data if any
            if (request.hasBody) {
                xhr.send(this.serialize(request.body));
            }
            else {
                xhr.send();
            }
        });
        // If a callback is defined then call it
        if (typeof callback === 'function') {
            p.then(callback);
        }
        // Returns the promise
        return p;
    }
    test() {
        return true;
    }
}
API.Headers = Headers;
API.Request = Request;
// Find script location if loaded with browser
(function () {
    if (typeof document === 'undefined') {
        return;
    }
    const scripts = document.getElementsByTagName('script');
    if (scripts.length > 0) {
        const src = scripts[scripts.length - 1].src;
        if (src && (src.includes('api.js') || src.includes('api.min.js'))) {
            API.dirname = src;
        }
    }
    // @ts-ignore
    if (typeof __filename === 'string') {
        // @ts-ignore
        API.dirname = __filename;
    }
})();

module.exports = API;
