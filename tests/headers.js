import Headers from "../src/headers";

test("Headers: constructor arguments", () => {
    expect(new Headers()).toBeInstanceOf(Headers);
    expect(Object.isFrozen(new Headers().headers)).toBe(false);
    expect(Object.isFrozen(new Headers({}, true).headers)).toBe(true);
    expect(new Headers({ test: "value" }).headers.test).toBe("value");
});

test("Headers: set headers and set returns headers instance", () => {
    const h = new Headers();
    expect(h.set("test", "value")).toBe(h);
    expect(h.headers.test).toBe("value");
});

test("Headers: get and getAll and getKeys", () => {
    const h = new Headers({
        test1: "first",
        test2: "second"
    });
    expect(h.get("test1")).toBe("first");
    expect(h.getAll().test2).toBe("second");
    expect(h.getKeys()).toEqual(["test1", "test2"]);
});

test("Headers: remove and has header", () => {
    const h = new Headers({
        test: "value"
    });
    expect(h.has("test")).toBe(true);
    expect(h.has("invalid")).toBe(false);
    expect(h.remove("test")).toBe(h);
    expect(h.get("test")).toBe(undefined);
});
