import Request from "../src/request";
import Headers from "../src/headers";

test("Request: new Request arguments", () => {
    expect(() => {
        new Request();
    }).toThrow();
    expect(new Request("url").url).toBe("url");
});

test("Request: domain and url", () => {
    const args = {
        domain: "domain",
        url: "/url"
    };
    const r = new Request(args);

    expect(r.url).toBe(args.domain + args.url);
});

test("Request: method default and given", () => {
    const def = "GET";
    const a = { url: "" };
    const b = { url: "", method: "get" };
    const c = { url: "", method: "post" };
    const d = { url: "", method: ["test"] };
    expect(new Request(a).method).toBe(def);
    expect(new Request(b).method).toBe("GET");
    expect(new Request(c).method).toBe("POST");
    expect(new Request(d).method).toBe(def);
});

test("Request: credentials default and given", () => {
    const def = true;
    const a = { url: "" };
    const b = { url: "", credentials: true };
    const c = { url: "", credentials: false };
    const d = { url: "", credentials: "test" };
    expect(new Request(a).credentials).toBe(def);
    expect(new Request(b).credentials).toBe(true);
    expect(new Request(c).credentials).toBe(false);
    expect(new Request(d).credentials).toBe(def);
});

test("Request: headers instanceof Headers", () => {
    expect(new Request({ url: "" }).headers).toBeInstanceOf(Headers);
});

test("Request: timeout default and given", () => {
    const def = 30;
    const a = { url: "" };
    const b = { url: "", timeout: 10 };
    const c = { url: "", timeout: 0 };
    const d = { url: "", timeout: "test" };
    expect(new Request(a).timeout).toBe(def);
    expect(new Request(b).timeout).toBe(10);
    expect(new Request(c).timeout).toBe(0);
    expect(new Request(d).timeout).toBe(def);
});

test("Request: body and hasBody", () => {
    const body = { someParameter: "Is just a parameter" };
    const r = new Request({ url: "", body: body });
    expect(r.body).toEqual(body);
    expect(r.hasBody).toBe(true);
    expect(new Request("").body).toBe(undefined);
    expect(new Request("").hasBody).toBe(false);
});
