import API from "../src/api";
import { XMLHttpRequest } from "xmlhttprequest";

self.XMLHttpRequest = XMLHttpRequest;

test("registerRoute routes", () => {
    const api = new API();
    api.registerRoute("test", {
        method: "get",
        url: "http://localhost"
    });
    expect(typeof api.routes.test).toBe("function");
});

test("registerRoute method creation", () => {
    const api = new API();
    api.registerRoute("test", {
        method: "get",
        url: "http://localhost"
    });
    expect(api.test).toBe(api.routes.test);
});

test("Handler undefined = 'default'", () => {
    expect(typeof new API()._handler).toBe("function");
});
test("Handler 'default'", () => {
    expect(typeof new API({ handler: "default" })._handler).toBe("function");
});
test("Handler custom function", () => {
    expect(new API({ handler: a => a })._handler("test")).toBe("test");
});

test("AJAX request returns promise", () => {
    const api = new API({
        domain: "https://www.google.com",
        handler: null
    });
    api.registerRoute("test", {
        method: "get",
        url: "/"
    });
    const p = api.test();
    expect(p).toBeInstanceOf(Promise);
});
test("AJAX response object", async () => {
    expect.assertions(3);
    const api = new API({
        domain: "https://www.google.com",
        handler: null
    });
    api.registerRoute("test", {
        method: "get",
        url: "/"
    });
    const res = await api.test();
    expect(res).toBeInstanceOf(Object);
    expect(res.status).toBe(200);
    expect(typeof res.raw).toBe("string");
});
